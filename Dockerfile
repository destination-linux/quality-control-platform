# Based on exmaple Nodejs Docker Webapp
# Found here: https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

FROM node:14

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Install project for production deployment
## See https://docs.npmjs.com/cli/v6/commands/npm-ci
RUN npm ci --only=production

# Bundle app source
COPY . .

# Build NextJS app
RUN npm run build

EXPOSE 3000

CMD [ "npm", "run", "start" ]
