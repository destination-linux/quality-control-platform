import Head from 'next/head'
import {Container, Row, Col} from "react-bootstrap";
import {GetStaticProps, NextPage} from "next";

interface Props {
    message: string;
}

const Page: NextPage<Props> = ({message}) => (
    <div>
        <Head>
            <title>Unnamed Quality Control</title>
            <link rel="icon" href="/favicon.ico"/>
        </Head>

        <Container>
            <Row>
                <Col>
                    <h1>Welcome to the QC Platform!</h1>
                </Col>
            </Row>
            <Row>
                <Col>
                    Message today: {message}
                </Col>
            </Row>
        </Container>

    </div>
)

export const getStaticProps: GetStaticProps<Props> = async () => (
    {
        props: {
            message: "Hello, QC!"
        }
    }
)

export default Page